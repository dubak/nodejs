# README #

* Examples and plays with Node.js
* Version 0.1
* [Link to repository](https://bitbucket.org/dubak/nodejs)

### How do I get set up? ###

* clone
* run from **terminal** following commands:


```
#!bash

cd your/path/to/nodejs/
node basics/server/helloworld.js

```