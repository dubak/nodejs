/**
 * @description By using process.nextTick(callback) instead of setTimeout(callback, 0), your callback runs
 *              immediately after all the events in the event queue have been processed, which is much sooner 
 *              (in the CPU time scale) than when the JavaScript timeout queue is activated.
 */

exports.blocking = function(){
    
    /**
     * @description In this case, the nextTick2 and timeout functions will never have the chance to run no matter how
     *              long you wait because the event loop is blocked by an infi nite cycle inside the first nextTick function.
     *              Even the scheduled timeout function that was supposed to run after one second does not run.
     */
    process.nextTick(function nextTick1() {
        console.log("next tick 1 running ...");
        var a = 0;
        while(true) {
            a++;
        }   
    });
    
    process.nextTick(function nextTick2() {
        console.log("next tick 2 running ...");
    });
    
    /**
     * 
     * @description Callback function goes into a scheduling queue, which, in this
     *              case, doesn’t even get to be dispatched due to nextTick1.
     */
    setTimeout(function timeout() {
        console.log("Set timeout called");
    }, 1000);
    
};


exports.nonblocking = function(number){
    
    PrivateStuff.introduce('nonblocking start');
    
    process.nextTick(function nextTick() {
        PrivateStuff.long(number);        
    });
    
    PrivateStuff.exit('nonblocking finish');
};

PrivateStuff = {
    
    counter: 1,
    
    introduce: function(word)
    {
        console.log('PrivateStuff - introduce talking: ' + word);
    },    
    
    long: function (loops)
    {
        var Start = new Date();
        console.log("Next tick long loop started at: " + Start.toString() );
        
        for (PrivateStuff.counter; PrivateStuff.counter < loops; PrivateStuff.counter++) 
        {
            // doing staff
        }
        
        var End = new Date();
        console.log('Next tick long loop ended at: ' + End.toString());

        // display time execution in terminal	
        console.log('Function loop execution time took: ' + (End.getTime() - Start.getTime()) + 'msec');        
    },
    
    exit: function(word)
    {
        console.log('PrivateStuff - exit talking: ' + word);
    }    
    
};