

exports.process = function(){

    var timeout = setTimeout(function A() {
        console.log("Function A - timed out!");
    }, 2000);

    setTimeout(function B() {
        console.log('Function B - before clearing A');
        clearTimeout(timeout);
        console.log('Function B - after clearing A');
    }, 1000);
    
};