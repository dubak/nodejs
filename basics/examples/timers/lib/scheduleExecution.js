
var period = 1000; // 1000 mili sec = 1 second

exports.process = function(){

    var interval = setInterval(function () {
        console.log("Set interval: tick " + PrivateCheck.counter);

        PrivateCheck.counter++;
        
        PrivateCheck.continue(interval);
    }, period);
    
    
};

PrivateCheck = {
    
    counter: 1,
    
    continue: function(interval)
    {
        if ( PrivateCheck.counter > 5 )
        {
            console.log('Counter - before clearing interval: ' + interval);
            clearInterval(interval);
            console.log('Counter - after clearing interval: ' + interval);
        }        
    }    
};