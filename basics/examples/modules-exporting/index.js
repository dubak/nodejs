
var functions = require('./modules/functions');
functions.printA(); // -> A
functions.printB(); // -> B
console.log(functions.pi); // -> 3.141592653589793

var CircleLib = require('./modules/circle.js');
var circle = new CircleLib(3);
console.log( 'Circle area: ' + circle.area() ); 