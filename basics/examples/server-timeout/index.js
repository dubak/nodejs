/*
 
 how to start NodeJS server in terminal:
 
 D:
 cd web/nodejs/path/to/project
 node index.js
 
 URL : localhost:3000/start
 */


// Load the http module to create an http server.
var http = require('http');
var url = require("url");
var count = 1;
// Configure our HTTP server to respond with Hello World to all requests.
var server = http.createServer(
    function (request, response) {

        var pathname = url.parse(request.url).pathname;

        // message into terminal
        console.log("Request for " + pathname + " received.");

        response.writeHead(200, {"Content-Type": "text/html"});

        var start = Date.now();

        setTimeout(function () {
            console.log(Date.now() - start);
            for (var i = 0; i < 1000000000; i++) {
            }

        }, 1000);

        setTimeout(function () {
            console.log(Date.now() - start);
        }, 2000);

        // zachytavanie vynimiek    
        //throw new Error('chyba');

        count++;
        response.write("<br />Pocet requestov: " + count + "");
        response.end("<br />End of response!");
    }

);

process.on('uncaughtException', function (err) {
    console.error('Odchytena chyba: ' + err);
    process.exit(1); // we exit manually
});

// Listen on port 3000, IP defaults to 127.0.0.1
server.listen(3000);

// Put a friendly message on the terminal
console.log("Server has started at http://127.0.0.1:3000/");
