/*
 
 how to start NodeJS server in terminal:
 
 D:
 cd web/nodejs/path/to/example/
 node index.js
 
 URL : localhost:3000/start
 */


// Load the http module to create an http server.
var http = require('http');
var url = require("url");

// Configure our HTTP server to respond with Hello World to all requests.
var server = http.createServer(
		function (request, response) {

			var pathname = url.parse(request.url).pathname;

			// message into terminal
			console.log("Request for " + pathname + " received.");

			response.writeHead(200, {"Content-Type": "text/html"});

			response.write(serveBooks());

			response.end("<br /><br />End of response!");
		}
);

// Listen on port 3000, IP defaults to 127.0.0.1
server.listen(3000);

// Put a friendly message on the terminal
console.log("Server has started at http://127.0.0.1:3000/");


var books = [
	'Metamorphosis',
	'Crime and punishment'
];

function serveBooks() {
	// I’m going to serve some HTML to the client
	var html = '<b>' + books.join('</b><br><b>') + '</b>';
	// I’m evil, and I’m going to change state!
	books = [];
	return html;
}
