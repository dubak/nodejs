
console.log('Module "clock" initializing...');

var util = require('util');
var EventEmitter = require('events').EventEmitter;

function Ticker( ) {
    console.log('Ticker instance initializing ...');
    
    var self = this;    
    var counter = 1;    
    
    setInterval(function() {
        self.emit('tick', counter++);
    }, 1000);    
};

Ticker.prototype.starter = function() {    
    console.log('Ticker start initializing ...');
    
    this.emit('start', "argument 1", "argument 2");    
};

util.inherits(Ticker, EventEmitter);

module.exports = Ticker;