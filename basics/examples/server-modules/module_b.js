// By default, each module exports an empty object {}.

// overide export to return function
module.exports = Person;

function Person (name) {
  this.name = name;
};

Person.prototype.talk = function () {
  return('my name is', this.name);
};