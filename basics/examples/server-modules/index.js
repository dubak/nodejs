/*
 
 how to start NodeJS server in terminal:
 
 D:
 cd web/nodejs/
 node apps/pr1.js
 
 URL : localhost:3000/start
 */


// Load the http module to create an http server.
var http = require('http');
var url = require("url");

var count = 1;

// Configure our HTTP server to respond to all requests.
var server = http.createServer(
	function (request, response) {

		var pathname = url.parse(request.url).pathname;

		// message into terminal
		console.log("Request for " + pathname + " received.");

		response.setHeader("Content-Language","sk");
		response.writeHead(200, {"Content-Type": "text/html;charset=utf-8"});
		
		response.write("<br />Process title: " + process.title + "<br />");

		console.log('first');
		// call this function in the most immediate future in an asynchronous way
		process.nextTick(function () {
			console.log('third - next tick');
		});
		console.log('second');

		// nacitaj modul
		var a = require('./module_a');
		console.log(a.name);
		console.log(a.data);
		console.log(a.getPrivate());

		var person = require('./module_b');
		var David = new person('Dávid');

		response.write("<br />Person hovori: " + David.talk() + "");

		response.write("<br />Pocet requestov: " + count + "");
		response.end("<br />End of response!");

		count++;
	}
);


// Listen on port 3000, IP defaults to 127.0.0.1
server.listen(3000);

// Put a friendly message on the terminal
console.log("Server has started at http://127.0.0.1:3000/");
