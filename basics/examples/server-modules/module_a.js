// By default, each module exports an empty object {}.

// adding property to export
exports.name = 'john';

exports.data = 'this is some data';

var privateVariable = 5;

exports.getPrivate = function () {
  return privateVariable;
};

