
var fs = require('fs');

fs.open('./article.txt', 'r', function opened(err, fd) {
    if (err) {
        throw err;
    }
    
    if ( fd === null ) {
        console.log('Could not open file for read!');
        return false;
    }
    
    var readBuffer = new Buffer(32), // 1024 -> 1kB
        bufferOffset = 0,
        bufferLength = readBuffer.length,
        filePosition = 0;
    
    function readByChunks ( )
    {
        fs.read(fd,
            readBuffer,
            bufferOffset,
            bufferLength,
            filePosition,
            function read(err, readBytes) {
                if (err) {
                    throw err;
                }
                console.log('Just read ' + readBytes + ' bytes');
                var data;
                if (readBytes < bufferLength) {
                    data = readBuffer.slice(0, readBytes);
                    console.log('Almost done ...');
                }
                else if ( readBytes > 0 && readBytes === bufferLength ) {
                    data = readBuffer;
                    console.log('Reading chunks from file, position: ' + filePosition);
                    console.log(data.toString('utf8'));
                    setTimeout( function(){
                        filePosition+=bufferLength;
                        // continue reading
                        readByChunks();                                            
                    }, 2000);
                }
                // done reading
                else {
                    fs.close(fd, function(err) {
                      if (err) throw err;
                    });
                    console.log('read 0 bytes, closing ...');
                    return;                                        
                }
            }
        );        
    }
    
    // start reading
    readByChunks();                    
});


