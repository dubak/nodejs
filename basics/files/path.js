var path = require('path');

var sJoin = path.join('/foo', 'bar', 'place/nice', 'lolol', '..');
console.log('Path - join: ' + sJoin); // => '/foo/bar/baz/asdf'

var sResolve = path.resolve('/foo/bar', './place');
console.log('Path - resolve: ' + sResolve); // => '/foo/bar/place'

var sResolve = path.resolve('wwwroot', 'static_files/png/', '../gif/image.gif');
// if the resulting path is not absolute, will prepend the current working directory to the path
console.log('Path - resolve: ' + sResolve); // => D:/web/nodejs/basics/wwwroot/static_files/gif/image.gif'

var sRelative = path.relative('/web/nodejs/basics', '/web/nodejs/apps/async');
console.log('Path - relative: ' + sRelative); // => ../apps/async

console.log('Path - DIR name: ' + path.dirname('/web/nodejs/basics/files/path.js') );

path.exists('/web/nodejs/basics', function(exists) {
    console.log('Path - exists:', exists); // => true
});

var existSync = path.existsSync('/abc/passwd');
console.log('Path - exists sync:', existSync); // => false