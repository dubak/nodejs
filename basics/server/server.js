
var http = require("http");

var url = require("url");

var router = require("./router");


/*
http.createServer(function(request, response) {
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.write("Hello David");
  response.end();
}).listen(8888);

// Put a friendly message on the terminal
console.log("Server running at http://127.0.0.1:8888/");
*/

function start() 
{
	function onRequest(request, response) 
	{		
	    var pathname = url.parse(request.url).pathname;
	    console.log("Request for " + pathname + " received.");
	    
	    router.route(pathname);
	    
	    response.writeHead(200, {"Content-Type": "text/plain"});
	    response.write("Hello David");
	    response.end();
	}

	http.createServer(onRequest).listen(8888);
	
	// Put a friendly message on the terminal
	console.log("Server has started at http://127.0.0.1:8888/");
}

exports.start = start;

