/*
 
 how to start NodeJS server in terminal:
 
 D:
 cd web/path/to/project/
 node async.js
 
 URL : localhost:3000/start
 */


// Load the http module to create an http server.
var host = '127.0.0.1';
var port = 3000;

var http = require('http');
var url = require('url');
var async = require('async');
var count = 1;

function runComputing(callback)
{
    setTimeout(function ( )
    {
        var i = 0;

        var Start = new Date();
        console.log('Function loop started at: ' + Start.toString());

        // do long term while cycle 2e9 is 2 000 000 000 which is 2 bilions	
        while (i < 2e9) {
            i++;
        }

        var End = new Date();
        console.log('Function loop ended at: ' + End.toString());

        // display time execution in terminal	
        console.log('Function loop execution time took: ' + (End.getTime() - Start.getTime()) + 'msec');

        callback(null, ["computed value: " + i]);

    }, 1000);
}

// Configure our HTTP server to respond with Hello World to all requests.
var server = http.createServer(
    function (request, response)
    {
        // parse requested URL path
        var pathname = url.parse(request.url).pathname;
        // message into terminal
        console.log("Request for " + pathname + " received.");

        async.parallel({
            computingResult: runComputing,
        },
            function (err, data) {

                console.log("Computing done: ", data);

            });

        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("<br />Number of requests: " + count + "");
        response.end("<br />End of response!");

        count++;
    }
);

// Listen on port 3000, IP defaults to 127.0.0.1
server.listen(port, host, function () {
    // Put a friendly message on the terminal
    console.log("Server has started at http://127.0.0.1:3000/");
});

