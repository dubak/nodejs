/** 
 * @description  How to start NodeJS server in Win 7 terminal
 *
 * @example cd D:/web/nodejs/path/to/example/
            node simpleWrite.js
 *
 * @link http://localhost:3000/
 * @author David Dubovsky http://www.dubak.sk/
 */

var host = '127.0.0.1';
var port = 3000;
var requestsCount = 1;

var http = require('http');
var url = require('url');
var fs = require('fs');

var errorMessage = 'default';

function writeDoc ( requestCnt ) {        
    fs.open('file.txt', 'w', 0666, function(error, fp) {        
        if ( fp !== null ) {
            var Start = new Date();
            fs.write(fp, 'Created by NodeJS program. Requests count: ' + requestCnt + ' Written at: ' + Start.toString(), null, 'utf-8', function(error, fp) {
                errorMessage = 'OK';
                fs.close(fp, function(error) {
                    errorMessage = 'Closed';
                });
            });            
        }
        else {
            errorMessage = 'Could not open file!';
        }
    });
}

// Configure our HTTP server to respond
var server = http.createServer(
		
	function (request, response) 
    {
        console.time("Server execution time");
        
        // parse requested URL path
        var pathname = url.parse(request.url).pathname;

		response.setHeader("Content-Language","sk");
		response.writeHead(200, {"Content-Type": "text/html;charset=utf-8"});
        
		response.write("<br />Process title: " + process.title + "<br />");
        response.write("<br />Request for " + pathname + " received. <br />");
        
        response.write("<br />Number of requests: "+requestsCount+"");
        
        writeDoc(requestsCount);
        console.log(errorMessage);
        
        response.end("<br />End of response!");
    
        requestsCount++;
        
        console.timeEnd("Server execution time");
    }    
);

// Listen on port 3000, IP defaults to 127.0.0.1
server.listen(port, host, function(){    
    // write message into the terminal
    console.log("Server has started at host: " + host + " and port: " + port);        
});
