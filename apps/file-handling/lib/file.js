/**
 * @description By default, each module exports an empty object {}.
 * @author David Dubovsky http://www.dubak.sk/
 */

// require modules
var fs = require('fs');

// overide export to return function
module.exports = File;

function File (name) {
    
  this.name = (!name || 0 === name.length) ? 'fileDefault.txt' : name;  
  
};

File.prototype.statusMessage = 'default, not modified';

File.prototype.write = function (requestCnt) {
    fs.open(this.name, 'w', 0666, function(error, fp) {        
        if ( fp !== null ) {
            File.prototype.statusMessage = 'File open OK';
            var Start = new Date();
            var text = 'Created by NodeJS program.\n\ ' +
                       'Requests count: ' + requestCnt + '\n\ ' +
                       'Written at: ' + Start.toString();
            
            fs.write(fp, text, null, 'utf-8', function(error) {
                File.prototype.statusMessage = 'File write OK';
                fs.close(fp, function(error) {
                    File.prototype.statusMessage = 'File closed';
                });
            });            
        }
        else {
            File.prototype.statusMessage = 'Could not open file!';
        }
    });
};

File.prototype.getStatusMessage = function(){
    return this.statusMessage;
};



