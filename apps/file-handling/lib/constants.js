// By default, each module exports an empty object {}.

// adding property to export
exports.host = '127.0.0.1';
exports.port = 3000;

var requestsCount = 1;
var errorMessage = 'default';

exports.getRequestsCount = function () {
  return requestsCount;
};

exports.getErrorMessage = function () {
  return errorMessage;
};

