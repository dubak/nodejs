/** 
 * @description  How to start NodeJS server in Win 7 terminal
 *
 * @example cd D:/web/nodejs/path/to/example/
            node simpleWritePrototype.js
 *
 * @link http://localhost:3000/start
 * @author David Dubovsky http://www.dubak.sk/
 */

// require modules
var http = require('http');
var url = require('url');
var appConstants = require('./lib/constants');
var appFile = require('./lib/file');

// variables
var requestsCounter = appConstants.getRequestsCount();

// Configure our HTTP server to respond
var server = http.createServer( function (request, response) {

    console.time("Server execution time");

    // parse requested URL path
    var pathname = url.parse(request.url).pathname;

    response.setHeader("Content-Language","sk");
    response.writeHead(200, {"Content-Type": "text/html;charset=utf-8"});

    response.write("<br />Process title: " + process.title + "<br />");
    response.write("<br />Absolute pathname of the executable that started the process: " + process.execPath + "<br />");
    response.write("<br />Request for " + pathname + " received. <br />");        
    response.write("<br />Number of requests: " + requestsCounter );

    var fileHandler = new appFile(' simpleWritePrototype.txt');
    fileHandler.write(requestsCounter);

    response.write('<br />Doc status message: ' + fileHandler.getStatusMessage() + '<br />');
    response.end("<br />End of response!");

    requestsCounter++;

    console.timeEnd("Server execution time");
});

// Listen on port 3000, IP defaults to 127.0.0.1
server.listen(appConstants.port, appConstants.host, function(){    
    // write message into the terminal
    console.log("Server has started at host: " + appConstants.host + " and port: " + appConstants.port );        
});
