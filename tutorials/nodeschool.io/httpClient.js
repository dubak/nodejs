/**
 * Created by bohac on 23.10.2015.
 */
var http = require('http');
var url = process.argv[2];

function httpGet() {

	http.get(url, function (response) {
		response.setEncoding("utf8");

		response.on("error", function (err) {
			console.log(err.message);
		})

		response.on("data", function (data) {
			console.log(data);
		})
	})
}

httpGet();