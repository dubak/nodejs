/**
 * Created by bohac on 23.10.2015.
 */
var net = require('net');
var port = process.argv[2];

var server = net.createServer(function (socket) {
	socket.end(getNow() + "\n");
})

function zeroFill(i){
	return (i< 10 ? 0 : '') + i;
}

function getNow() {
	var d = new Date();
	var datum = d.getFullYear() + "-" +
		zeroFill(d.getMonth() + 1) + "-" +
		zeroFill(d.getDate()) + " " +
		zeroFill(d.getHours()) + ":" +
		zeroFill(d.getMinutes());
	return datum;
}

function serverListen() {
	server.listen(Number(port));
}

serverListen();