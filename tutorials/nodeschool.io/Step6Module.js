/**
 * Created by bohac on 23.10.2015.
 */
var fs = require('fs');
var path = require('path');

module.exports = function (dir, extension, callback) {
	fs.readdir(dir, function (err, files) {
		if (err) {
			return callback(err);
		}
		var resultData = [];
		files.forEach(function (file) {
			var fileEnd = path.extname(file);
			if (fileEnd === ("." + extension)) {
				resultData.push(file);
			}
		});

		callback(null, resultData);
	});
}
